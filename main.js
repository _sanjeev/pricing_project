const toggle = document.getElementById('checkbox');
const basicPrice = document.getElementById('basicPrice');
const professionalPrice = document.getElementById('professionalPrice');
const masterPrice = document.getElementById('masterPrice');
const item2 = document.getElementById('secondDiv');
const item1 = document.getElementById('firstDiv');
const item3 = document.getElementById('thirdDiv');
console.log(item2);
item1.addEventListener('mouseover', () => {
  item1.className=' item1 professional';
  item2.className='item1';
});
item1.addEventListener("mouseout",(e)=>{
  console.log('hello');
  item1.className='item1';
  item2.className=' item1 professional';
})


item3.addEventListener('mouseover', () => {
  item3.className=' item1 professional';
  item2.className='item1';
});
item3.addEventListener("mouseout",(e)=>{
  console.log('hello');
  item3.className='item1';
  item2.className=' item1 professional';
})












function annuallyMonthly(data) {
  if (toggle.checked) {
    basicPrice.textContent = data.monthly.basicPrice;
    professionalPrice.textContent = data.monthly.professionalPrice;
    masterPrice.textContent = data.monthly.masterPrice;
  } else {
    basicPrice.textContent = data.annually.basicPrice;
    professionalPrice.textContent = data.annually.professionalPrice;
    masterPrice.textContent = data.annually.masterPrice;
  }
}

async function toggling() {
  const response = await fetch('./data.json');
  const data = await response.json();
  toggle.addEventListener('change', () => {
    annuallyMonthly(data);
  });
}

toggling();
